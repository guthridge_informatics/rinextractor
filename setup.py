#!/usr/bin/env python
import sys

if sys.version_info < (3,):
    sys.exit("RINExtractor requires Python >= 3.6")
from pathlib import Path
from setuptools import setup, find_packages
import versioneer

try:
    from rinextractor import __author__, __email__
except ImportError:  # Deps not yet installed
    __author__ = __email__ = ""

setup(
    name="RINExtractor",
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    description="extract RIN values from Bioanalyzer files",
    long_description=Path("README.rst").read_text("utf-8"),
    url="https://gitlab.com/milothepsychic/rinextractor",
    author=__author__,
    author_email=__email__,
    license="BSD3",
    python_requires=">=3.6",
    install_requires=[
        l.strip() for l in Path("requirements.txt").read_text("utf-8").splitlines()
    ],
    classifiers=[
        "Development Status :: 3 - Production",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Operating System :: MacOS :: MacOS X",
        "Operating System :: POSIX :: Linux",
        "Topic :: Scientific/Engineering :: Bio-Informatics",
    ],
    extras_require=dict(doc=["sphinx", "sphinx_rtd_theme", "sphinx_autodoc_typehints"]),
    packages=find_packages(),
    include_package_data=True,
    entry_points={"console_scripts": ["rinextractor = rinextractor.__main__:main"]},
    keywords="",
    package_dir={"rinextractor": "rinextractor"},
)
