from rinextractor.__main__ import main
from rinextractor.fileprocessor import process_results_file

__author__ = ("Miles Smith",)
__email__ = "miles-smith@omrf.org"


__doc__ = """\
rinextractor
============

Extracts RIN values from Agilent Bioanalyzer 2100 CSV output
"""

from ._version import get_versions

__version__ = get_versions()["version"]
del get_versions
