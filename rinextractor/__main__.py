#!/usr/bin/env python3

import os
import glob
import click
import pandas as pd

from rinextractor.fileprocessor import process_results_file


@click.command(name="main",)  # no_args_is_help=True
@click.option(
    "--source_folder",
    "-f",
    help="Folder containing CSVs from which to extact values.",
    default=None,
)
@click.option(
    "--recursive",
    "-r",
    help="Scan `source_folder` recursively for CSVs to extact values from.",
    default=False,
    type=bool,
    is_flag=True,
)
@click.option(
    "--input_file",
    "-i",
    help="File containing values to extract.  If '--source_folder' is provided, this value is ignored.",
    default=None,
)
@click.option(
    "--output",
    "-o",
    help="File to write values to.  If the file already exists, results are appended at the bottom.",
    default="RIN_results.csv",
    type=str,
)
def main(
    source_folder=None, recursive=False, input_file=None, output="rinresults.csv",
) -> None:
    """Extract RIN and concentration values from the CSV files written by an Agilent Bioanalyzer 2100
    \f
    Parameters
    ==========
    source_folder : :class:~`typing.Optional`[`str`]
        Folder containing CSVs from which to extract values.
    recursive : `bool`
        Scan `source_folder` recursively for CSVs to extract values from.
    input_file : :class:~`typing.Optional`[`str`]
        File containing values to extract.  If '--source_folder' is provided, this value is ignored.
    output : `str`
        File to write values to.  If the file already exists, results are appended at the bottom.

    Returns
    =======
    None
    """

    if source_folder is not None:
        if not os.path.exists(source_folder):
            raise AssertionError(
                f"The {source_folder} directory was not found.  Maybe try providing the full path?"
            )
        # search for all *.csv files and attempt to extract the information from them.
        if recursive:
            rin_files = glob.glob(
                pathname=f"{source_folder}/**/*.csv", recursive=recursive
            )
        else:
            rin_files = glob.glob(
                pathname=f"{source_folder}/*.csv", recursive=recursive
            )
        rin_df = pd.concat(map(process_results_file, rin_files), axis=0)
    elif input_file is not None:
        if not os.path.isfile(input_file):
            raise AssertionError(f"{input_file} was not found.")
        else:
            rin_df = process_results_file(input_file)

    rin_df.to_csv(output)


if __name__ == "__main__":
    main()
