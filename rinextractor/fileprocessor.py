import os
import pandas as pd


def process_results_file(file_name) -> pd.DataFrame:
    with open(file_name, "rb") as f:
        alpha = [line.decode(errors="ignore").strip("\r\n") for line in f]

    try:
        header = [
            alpha[_].strip("\r\n").split(",") for _ in range(0, 14) if alpha[_] != " "
        ]
        header = {_[0]: _[1:] for _ in header}
        alpha = alpha[14:]
        sample_name = [_.strip("Sample Name,") for _ in alpha if "Sample Name" in _]
        rna_conc = [_.split(",")[1] for _ in alpha if "RNA Concentration" in _]
        RIN = [
            _.split(",")[1].split(" ")[0]
            for _ in alpha
            if "RNA Integrity Number (RIN)" in _
        ]
    except:
        return None
    try:
        if len(RIN) == 0:
            return None
        else:
            rin_df = pd.DataFrame(
                {
                    "sample_name": sample_name,
                    "RNA_conc": rna_conc,
                    "RIN": RIN,
                    "DataFileName": header["Data File Name"][0],
                    "RawData": os.path.abspath(file_name),
                    "Timestamp": pd.to_datetime(
                        "".join(header["Date Last Modified"][1:]).strip('"').lstrip()
                    ),
                }
            )
        return rin_df
    except ValueError:
        print(
            f"There was an issue with {file_name}.  You will want to extract the values manually."
        )
        return None
